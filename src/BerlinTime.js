class BerlinTime {
  constructor(
    singleMinutesRow,
    fiveMinutesRow,
    singleHourRow,
    fiveHourRow,
    secondsRow,
  ) {
    this.singleMinutesRow = singleMinutesRow;
    this.fiveMinutesRow = fiveMinutesRow;
    this.singleHourRow = singleHourRow;
    this.fiveHourRow = fiveHourRow;
    this.secondsRow = secondsRow;
  }

  getSingleMinutesRow() {
    return this.singleMinutesRow;
  }

  getFiveMinutesRow() {
    return this.fiveMinutesRow;
  }

  getSingleHourRow() {
    return this.singleHourRow;
  }

  getFiveHourRow() {
    return this.fiveHourRow;
  }

  getSecondsRow() {
    return this.secondsRow;
  }

  toString() {
    return this.secondsRow
      + this.fiveHourRow
      + this.singleHourRow
      + this.fiveMinutesRow
      + this.singleMinutesRow;
  }
}

module.exports = BerlinTime;
