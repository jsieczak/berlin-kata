const BerlinTime = require('./BerlinTime');

function toBerlinTime(digitalTime) {
  const [hours, minutes, seconds] = digitalTime.split(':');
  const minutesInt = Number(minutes);
  const singleMinutesRow = 'Y'.repeat(minutesInt % 5).padEnd(4, 'O');
  const fiveMinutesRow = 'Y'.repeat(minutesInt / 5)
    .split('')
    .map((el, index) => index % 3 === 2 ? 'R' : el)
    .join('')
    .padEnd(11, 'O');
  const singleHourRow = 'R'.repeat(hours % 5).padEnd(4, 'O');
  const fiveHourRow = 'R'.repeat(hours / 5).padEnd(4, 'O');
  const secondsRow = seconds % 2 === 0 ? 'Y' : 'O';
  return new BerlinTime(
    singleMinutesRow, fiveMinutesRow,
    singleHourRow, fiveHourRow, secondsRow,
  );
}

function toDigitalTime(berlinTime) {
  return '00:00:00';
}

module.exports = {
  toBerlinTime,
  toDigitalTime,
};
