/* global suite, test */
const assert = require('assert');
const BerlinTime = require('../src/BerlinTime');
const { toBerlinTime, toDigitalTime } = require('../src/time-converter');

suite('converters', () => {
  suite('Digital time to Berlin time conversion', () => {
    test('single minutes row', () => {
      assert.equal(toBerlinTime('00:00:00').getSingleMinutesRow(), 'OOOO');
      assert.equal(toBerlinTime('23:59:59').getSingleMinutesRow(), 'YYYY');
      assert.equal(toBerlinTime('12:32:00').getSingleMinutesRow(), 'YYOO');
      assert.equal(toBerlinTime('12:35:00').getSingleMinutesRow(), 'OOOO');
      assert.equal(toBerlinTime('12:34:00').getSingleMinutesRow(), 'YYYY');
      assert.equal(toBerlinTime('12:38:00').getSingleMinutesRow(), 'YYYO');
    });
    test('five minutes row', () => {
      assert.equal(toBerlinTime('00:00:00').getFiveMinutesRow(), 'OOOOOOOOOOO');
      assert.equal(toBerlinTime('23:59:59').getFiveMinutesRow(), 'YYRYYRYYRYY');
      assert.equal(toBerlinTime('12:04:00').getFiveMinutesRow(), 'OOOOOOOOOOO');
      assert.equal(toBerlinTime('12:23:00').getFiveMinutesRow(), 'YYRYOOOOOOO');
      assert.equal(toBerlinTime('12:35:00').getFiveMinutesRow(), 'YYRYYRYOOOO');
    });
    test('single hour row', () => {
      assert.equal(toBerlinTime('00:00:00').getSingleHourRow(), 'OOOO');
      assert.equal(toBerlinTime('23:59:59').getSingleHourRow(), 'RRRO');
      assert.equal(toBerlinTime('02:04:00').getSingleHourRow(), 'RROO');
      assert.equal(toBerlinTime('08:23:00').getSingleHourRow(), 'RRRO');
      assert.equal(toBerlinTime('14:35:00').getSingleHourRow(), 'RRRR');
    });
    test('five hour row', () => {
      assert.equal(toBerlinTime('00:00:00').getFiveHourRow(), 'OOOO');
      assert.equal(toBerlinTime('23:59:59').getFiveHourRow(), 'RRRR');
      assert.equal(toBerlinTime('02:04:00').getFiveHourRow(), 'OOOO');
      assert.equal(toBerlinTime('08:23:00').getFiveHourRow(), 'ROOO');
      assert.equal(toBerlinTime('16:35:00').getFiveHourRow(), 'RRRO');
    });
    test('seconds row', () => {
      assert.equal(toBerlinTime('00:00:00').getSecondsRow(), 'Y');
      assert.equal(toBerlinTime('00:00:01').getSecondsRow(), 'O');
      assert.equal(toBerlinTime('23:59:59').getSecondsRow(), 'O');
      assert.equal(toBerlinTime('23:59:24').getSecondsRow(), 'Y');
    });
    test('toString', () => {
      assert.equal(toBerlinTime('00:00:00').toString(), 'YOOOOOOOOOOOOOOOOOOOOOOO');
      assert.equal(toBerlinTime('23:59:59').toString(), 'ORRRRRRROYYRYYRYYRYYYYYY');
    });
  });
  suite('Berlin time to digital time conversion', () => {
    test('converts to digital time as string', () => {
      assert.equal(toDigitalTime(new BerlinTime('OOOO', 'OOOOOOOOOOO', 'OOOO', 'OOOO', 'Y')), '00:00:00');
      assert.equal(toDigitalTime(new BerlinTime('RRRR', 'RRROYYRYYRY', 'YRYY', 'YYYY', 'O')), '23:59:59');
    });
  });
});
